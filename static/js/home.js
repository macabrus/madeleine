// Attaching Madeleine galleries to DOM elements
import Gallery from './Gallery.js';

$(function () {
	console.log('Hererrerre');
	$('.madeleine-gallery-placeholder').each(function () {
		new Gallery($(this).attr('madeleine-path')).init().then($gallery => {
			$(this).append($gallery);
		});
	});
});

// Initializes and opens PhotoSwipe
$('.action-slideshow').on('click', () => {
	// build items array
	var items = $('.madeleine-gallery img').toArray().map(img => {
		console.log($(img).attr('src'));
		return {
			src: $(img).attr('src'),
			w: img.naturalWidth,
			h: img.naturalHeight,
		}
	});
	console.log(items);
	// define options (if needed)
	var options = {
		// optionName: 'option value'
		// for example:
		closeEl: true,
		captionEl: true,
		fullscreenEl: true,
		zoomEl: true,
		shareEl: true,
		counterEl: true,
		arrowEl: true,
		preloaderEl: true,
		showAnimationDuration: 1,
		hideAnimationDuration: 1,
		index: 0, // start at first slide
		bgOpacity: 0.7,
	};
	var pswpElement = document.querySelectorAll('.pswp')[0];
	var gallery = new PhotoSwipe(pswpElement, false, items, options);
	gallery.init();
});