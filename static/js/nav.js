// add padding top to show content behind navbar
$('body').css('padding-top', $('.navbar').outerHeight() + 'px')


// ALTERNATE EFFECT: https://stackoverflow.com/questions/7385068/freeze-header-until-irrelevant-html-css-and-js/
// iOS like
// detect scroll top or down
if ($('.smart-scroll').length > 0) { // check if element exists
	var last_scroll_top = 0;
	$(window).on('scroll', function () {
		scroll_top = $(this).scrollTop();
		if (scroll_top <= 0 || scroll_top < last_scroll_top) {
			$('.smart-scroll').removeClass('scrolled-down').addClass('scrolled-up');
		}
		else {
			$('.smart-scroll').removeClass('scrolled-up').addClass('scrolled-down');
		}
		last_scroll_top = scroll_top;
	});
}

$('.logout').on('click', function () {
	$.ajax({
		type: "POST",
		url: '/logout',
		success: window.location.href = '/login',
	})
})
