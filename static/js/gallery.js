import API from './API.js';
import Photo from './Photo.js';

// shared template for instantiating Photo
const templateUrl = '/static/html/gallery.html';
var template;
// API access object
var api = new API();

// Photo gallery
export default class Gallery {
	constructor(path) {
		this.path = path;
	}

	async init() {
		console.log('Initializing Madeleine Gallery');
		// waiting for template if necessary
		await fetchTemplate(this);
		// attaching data to jquery object
		// so it is accessible later
		this.$gallery.data('madeleineGallery', this);
		this.sectionTemplate = this.$gallery.find('.madeleine-section-template').html();
		//TODO - make as many sections as necessary
		// for now, just appending single dummy section 
		var $dummySection = $(this.sectionTemplate);
		$dummySection.find('.madeleine-section-title').text('Dummy Section');
		console.log($dummySection);
		var entries = [];
		// waiting for data
		await api.list(this.path, { files: true, meta: true, dummy: true }).then(async files => {
			// TODO
			entries = files;
			// grouping files in groups by months
			await Promise.all(files.groupBy(meta => {
				var created = new Date(meta.created);
				return created.getFullYear() * 12 + created.getMonth();
			}).map(async group => {
				var $section = $(this.sectionTemplate);
				const d = new Date(group[0].created);
				const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
				const mo = new Intl.DateTimeFormat('hr', { month: 'long' }).format(d);
				const da = new Intl.DateTimeFormat('hr', { day: '2-digit' }).format(d);
				var title = `${mo} ${ye}.`;
				$section.find('.madeleine-section-title').text(title);
				await Promise.all(group.map(async file =>
					await new Photo(file.path, file).init().then($el =>
						$section.find('.madeleine-section-body').append($el)
					)
				)).then(() => {
					this.$gallery.find('.madeleine-sections').append($section);
					console.log(this.$gallery.find('.madeleine-section'));
					this.refreshMasonry(this.$gallery.find('.madeleine-section-body'));
				});
			}));
		});
		this.$gallery.find('.madeleine-spinner').hide();
		this.$gallery.find('.fa-photo').toggleClass('d-none', !!entries.length);
		this.$gallery.find('.gallery-end').text(entries.length ? 'Nema više fotografija.' : 'Nema fotografija za prikaz.');
		return this.$gallery;
	}

	destroy() {
		this.$gallery.removeData('madeleineGallery');
		this.$gallery.remove();
	}

	refreshMasonry($sections) {
		$sections.each(function () {
			var $section = $(this);
			console.log($section);
			// Layout appropriately with Masonry.js
			$section.masonry({
				itemSelector: '.grid-item',
			});
			// re-layout Masonry
			$section.imagesLoaded().progress(function () {
				$section.masonry();
			});
		});
	}
}

// fetches necessary template
// it is cached as static variable
async function fetchTemplate(galleryObj) {
	if (!template)
		return await fetch(templateUrl)
			.then(res => res.text())
			.then(html => {
				galleryObj.$gallery = $(html);
				return galleryObj.$gallery;
			}, err => err);
	return new Promise((resolve, reject) => {
		galleryObj.$gallery = template.clone();
		resolve(galleryObj.$gallery);
	});
}

// util func
Array.prototype.groupBy = function (key) {
	var tmp = this.reduce(function (rv, x) {
		(rv[key(x)] = rv[key(x)] || []).push(x);
		return rv;
	}, {});
	var newArr = [];
	for (const [key, value] of Object.entries(tmp))
		newArr.push(value);
	return newArr;
}

console.log('Grouped:', [{ name: 'a' }, { name: 'a' }, { name: 'b' }, { name: 'b' }].groupBy(e => e.name))
