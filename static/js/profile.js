function getProfileInfo($details, username) {
	fetch('/api/profile?' + new URLSearchParams({
		user: username
	})).then(res => res.json()).then(json => {
		$details.find('.username').text(json.user);
		$details.find('.first-name').text(json.firstName);
		$details.find('.last-name').text(json.lastName);
	});
}

(async () => {
	getProfileInfo($('.profile'), (await (await fetch('/api/whoami')).json()).user);
})()