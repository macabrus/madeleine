import API from './API.js';

// shared template for instantiating Photo
const templateUrl = '/static/html/photo.html';
var template;
// API access object
var api = new API();

// wrapper around photo object
export default class Photo {
	constructor(path, meta) {
		this.path = path;
		this.meta = meta;
		//console.log(this.path);
	}

	// refreshes UI elements
	async init() {
		// waiting for template if necessary
		await fetchTemplate(this);
		// attaching data to jquery object
		// so it is accessible later
		this.$photo.data('madeleinePhoto', this);

		// wiring elements to template instance
		this.$loveBtn = this.$photo.find('.madeleine-action-love');
		this.$addBtn = this.$photo.find('.madeleine-action-add');
		this.$tagBtn = this.$photo.find('.madeleine-action-tag');

		this.$img = this.$photo.find('img');
		this.$tags = this.$photo.find('.madeleine-tags');
		this.$tagTemplate = this.$photo.find('template.tag');
		this.$heart = this.$photo.find('.madeleine-heart');

		// waiting for data
		// if not provided in constructor
		if (!this.meta) {
			await this.refetchMeta();
		}
		if (!this.meta.mime || !this.meta.mime.startsWith('image')) {
			this.$photo.find('img').css('visibility', 'hidden');
			this.$photo.find('.overlay').css('visibility', 'hidden');
			this.$heart.css('visibility', 'hidden');
			this.$tags.css('visibility', 'hidden');
			this.$photo.find('.madeleine-photo-error-overlay').toggleClass('d-none', false);
			this.$photo.find('.madeleine-photo-error-text').text(`Greška u prikazu ${this.meta.name}`);
		}
		// loading tags
		this.$tags.empty();
		this.meta.tags.forEach(tag => {
			var $tag = $(this.$tagTemplate.html());
			$tag.find('.label').text(tag.label);
			$tag.addClass(`badge-${tag.type}`);
			this.$tags.append($tag);
		});
		// toggling icons
		this.$heart
			.toggleClass('d-none', !this.meta.loved)
			.toggleClass('fa fa-heart', this.meta.loved);
		this.$loveBtn
			.toggleClass('fa fa-2x', true)
			.toggleClass('fa-heart', this.meta.loved)
			.toggleClass('fa-heart-o', !this.meta.loved);
		this.$addBtn.addClass('fa fa-plus fa-2x');
		this.$tagBtn.addClass('fa fa-tag fa-2x');
		// this triggers image loading if its not already loaded
		if (this.$img.attr('src') != this.meta.url)
			this.$img.attr('src', this.meta.url);
		// return itself after initialized
		// so it can be embeded in DOM
		return this.$photo;
	}

	// destroys jQuery element
	destroy() {
		this.$photo.removeData('madeleinePhoto');
		this.$photo.remove();
	}

	async refetchMeta() {
		return await api.meta(this.path).then(meta => {
			this.meta = meta;
			return meta;
		});
	}
}

// fetches necessary template
// it is cached as static variable
async function fetchTemplate(photoObj) {
	if (!template)
		return await fetch(templateUrl)
			.then(res => res.text())
			.then(html => {
				photoObj.$photo = $(html);
				return photoObj.$photo;
			}, err => err);
	return new Promise((resolve, reject) => {
		photoObj.$photo = template.clone();
		resolve(photoObj.$photo);
	});
}