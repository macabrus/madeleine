// API for listing dirs of current context dir
// Context is embeded by EJS template in q attr
// params: element to list into, path to request from server
import API from './API.js';
const api = new API();

function refreshDirs($dirlist, path) {
	api.list(path, { dirs: true, meta: true })
		.then(dirs => {
			console.dir(dirs);
			$dirlist.find('.fa-spinner').hide();
			if (!dirs.length) {
				$dirlist.find('.madeleine-spinner').toggleClass('d-none');
				$dirlist.find('.fa-folder-open').toggleClass('d-none');
				$dirlist.find('.dir-end').text('Nema mapa za prikaz.');
				return;
			}
			else {
				$dirlist.find('.list-label').hide();
			}
			dirs.forEach(dir => {
				var $dir = $($dirlist.find('template.dir').html());
				$dir.attr('href', `/?path=${dir.path}`).append(dir.name);
				$dirlist.find('.dir-container').append($dir);
			});
		});
	// fetch('/api/list?' + new URLSearchParams({
	// 	dirs: 'y',
	// 	path: path
	// })).then(res => res.json()).then((json) => {
	// 	// remove spinner when everything loaded
	// 	$dirlist.find('.fa-spinner').hide();
	// 	$dirlist.find('.list-label').hide();
	// 	if (json.length == 0) {
	// 		$dirlist.find('.fa-folder-open').toggleClass('d-none');
	// 		$dirlist.find('.dir-end').text('Nema mapa za prikaz.');
	// 		return;
	// 	}
	// 	json.forEach(element => {
	// 		
	// 	});
	// });
}

var $dirs = $('.dirs');
refreshDirs($dirs, $dirs.attr('q'));