
// fetches necessary template
// it is cached as static variable
async function getTemplate(templateUrl) {
	if (!localStorage.getItem('templates'))
		localStorage.setItem('templates', JSON.stringify([]))
	if (!JSON.parse(localStorage.getItem('templates'))[templateUrl])
		return await fetch(templateUrl)
			.then(res => res.text())
			.then(html => {
				var templates = localStorage.getItem('templates');
				templates[templateUrl] = html;
				localStorage.setItem('templates', templates);
				return html;
			}, err => err);
	return new Promise((resolve, reject) => {
		resolve(localStorage.getItem('templates')[templateUrl]);
	});
}