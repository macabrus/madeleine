// Client side API for querying server
// Note: every method returns a JSON as Promise
export default class API {

	// It only sends JSON and receives JSON as response
	constructor() {
		this.headers = {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}

	// adds photos into collections
	pin(paths, collections, options) {
		var opts = { action: 'add' };
		Object.assign(opts, options);
		return fetch('/api/pin', {
			method: 'POST',
			headers: this.headers,
			body: JSON.stringify({
				options: opts,
				paths: paths,
				collections: collections
			})
		}).then(res => res.json());
	}

	// removes photos from collections
	unpin(paths, collections, options) {
		var opts = { action: 'remove' };
		Object.assign(opts, options);
		return pin(paths, collections, opts);
	}

	// adds tags to photos
	tag(paths, tags, options) {
		var opts = { action: 'add' };
		Object.assign(opts, options);
		return fetch('/api/tag', {
			method: 'POST',
			headers: this.headers,
			body: JSON.stringify({
				options: opts,
				paths: paths,
				tags: tags
			})
		}).then(res => res.json());
	}

	// removes tags from photos
	untag(paths, tags, options) {
		var opts = { action: 'remove' };
		Object.assign(opts, options);
		return this.tag(paths, tags, opts);
	}

	// returns list of profiles with info
	profiles(profiles, options) {
		var opts = {};
		Object.assign(opts, options);
		return fetch('/api/profile', {
			method: 'POST',
			headers: this.headers,
			body: JSON.stringify({
				profiles: profiles,
				options: opts
			})
		}).then(res => res.json());
	}

	profile(profile, options) {
		var opts = {};
		Object.assign(opts, options);
		return this.profiles([profile], opts).then(arr => arr[0]);
	}

	// fetches metadata for array of paths
	metaAll(paths, options) {
		var opts = {};
		Object.assign(opts, options);
		return fetch('/api/meta', {
			method: 'POST',
			headers: this.headers,
			body: JSON.stringify({
				paths: paths,
				options: opts
			})
		}).then(res => res.json());
	}

	// gets metadata for provided paths
	meta(path, options) {
		var opts = {};
		Object.assign(opts, options);
		return this.metaAll([path], opts).then(arr => arr[0]);
	}

	// lists provided directories paths
	listAll(paths, options) {
		var opts = {};
		Object.assign(opts, options);
		return fetch('/api/list', {
			method: 'POST',
			headers: this.headers,
			body: JSON.stringify({
				options: opts,
				paths: paths,
			})
		}).then(res => res.json());
	}

	// lists a directory from remote
	list(path, options) {
		var opts = {};
		Object.assign(opts, options);
		return this.listAll([path], opts).then(arr => arr[0]);
	}
}
