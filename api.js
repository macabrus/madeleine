// API routes
const express = require('express');
const { Router } = express;
const api = Router();
const fsutils = require('./fsutils');
const db = require('./db');
const validator = require('./validators');

// middleware to parse json POST body
api.use(express.json());

// api for listing directories for every path in array of paths
api.post('/list', validator('list'), function (req, res) {
	fsutils.listAll(req.body.paths, req.body.options)
		.then(
			arr => res.json(arr),
			err => {
				console.error(err);
				res.json({ errors: [{ err: 'Wrong body format' }] });
			}
		);
});

// fetching FS and DB info for list of paths
api.post('/meta', validator('meta'), function (req, res) {
	fsutils.getMergedMeta(req.body.paths, req.body.options)
		.then(arr => res.json(arr), err => res.json(err));
});

// api for getting actual file
api.get('/file', validator('get'), function (req, res) {
	res.sendFile(fsutils.getFsPath(req.query.path));
});

api.get('/pin', validator('pin'), function (req, res) {
	db.addToCollection();
});

api.post('/add', validator('add'), function (req, res) {
	var action = {};
	var collection = {};
	var path = stripPath(req.query.path);
	collection[`collections.${req.query.collection}.paths`] = path;
	action.$addToSet = collection;
	console.dir(action);
	usrdb.update(
		{ user: req.session.user }, // matching entries with
		action, // update to be made
		{}, // options
		(err, doc) => {
			if (err)
				console.error(err);
			else
				console.log(doc);
			res.json({
				toast: true,
				class: 'success',
				msg: `${path} uspješno dodan u kolekciju ${req.query.collection}`
			});
		}
	);
});

api.post('/remove', validator('add'), function (req, res) {
	var action = {};
	var collection = {};
	var path = stripPath(req.query.path);
	collection[`collections.${req.query.collection}.paths`] = path;
	action.$pull = collection;
	console.dir(action);
	usrdb.update(
		{ user: req.session.user }, // matching entries with
		action, // update to be made
		{}, // options
		(err, doc) => {
			if (err)
				console.error(err);
			else
				console.log(doc);
			res.json({
				toast: true,
				class: 'success',
				msg: `${path} uspješno izbrisan iz kolekcije ${req.query.collection}`
			});
		}
	);
});

api.post('/tag', validator('tag'), function (req, res) {
	res.json({
		toast: true,
		class: 'success',
		msg: 'Fotografija je uspješno tagirana!'
	})
});

api.post('/untag', validator('untag'), function (req, res) {
	res.json({ untag: 'dmgkfmkgdfmg' });
});

// lock dir/file action
api.post('/lock', validator('lock'), function (req, res) {
	res.json({
		err: 'Path must be provided'
	});
});

// unlock dir/file action
api.post('/unlock', validator('unlock'), function (req, res) {
	res.send(`Unlocking ${req.path}`);
});

api.get('/profile', validator('profile'), function (req, res) {
	db.getUser(req.session.user).then(json => res.json(json), err => res.json(err));
});

// list collections of some user...
api.get('/collections', validator('collections'), function (req, res) {

});

//util func
api.get('/whoami', function (req, res) {
	res.json({
		user: req.session.user
	});
});

module.exports = api;