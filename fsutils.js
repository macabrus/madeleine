// Utils for File System operations performed by Madeleine

const { join, resolve, relative, basename } = require('path');
const fs = require('fs');
const { idxdb, usrdb } = require('./db');
const FileType = require('file-type');
const db = require('./db');

// TODO !!!
// Should be set as environment variable
const ROOT_DIR = resolve('./root')

// resolves to sys path and returns it only if it is in same subdir
exports.getFsPath = function (path) {
	var resolved = resolve(ROOT_DIR, exports.stripPath(path));
	if (resolved.startsWith(ROOT_DIR))
		return resolved
	else
		return null
}

exports.isDirectory = function (path) {
	var sysPath = exports.getFsPath(path);
	return fs.statSync(sysPath).isDirectory();
}

exports.isFile = function (path) {
	var sysPath = exports.getFsPath(path);
	return fs.statSync(sysPath).isFile();
}

// remove leading and trailing slashes
exports.stripPath = function (path) {
	return path.trim().replace(/(^\/|\/$)/g, '');
}

// returns file/dir meta information,
// could take some time to read MIME type so its a Promise
exports.getMetadata = async function (path, options) {
	var opts = {};
	Object.assign(opts, options);
	var itemPath = exports.getFsPath(path);
	var itemStat = fs.statSync(itemPath);
	var meta = {
		created: Date.parse(itemStat.birthtime),
		isDir: itemStat.isDirectory(),
		isFile: itemStat.isFile(),
		name: basename(path),
		path: relative(ROOT_DIR, itemPath),
		mime: await FileType.fromFile(itemPath)
			.then(
				res => (res ? res.mime : null),
				err => null
			)
	}
	if (opts.dummy) {
		meta.tags = [
			{
				type: 'warning',
				label: 'bonjour'
			},
			{
				type: 'success',
				label: 'hi'
			},
			{
				type: 'info',
				label: 'hello'
			},
		];
		meta.collections = ['loved', 'cool', 'wtf'];
		meta.loved = meta.collections.includes('loved');
		meta.url = `/api/file?path=${meta.path}`;
	}
	return meta;
}

// lists dir with necessary child metadata
exports.list = async function (path, options) {
	var opts = { files: false, dirs: false, meta: false };
	Object.assign(opts, options);
	return await Promise.all(
		fs.readdirSync(exports.getFsPath(path), { encoding: 'utf8' })
			.map(name => join(path, name))
			.filter(rel => // filtering only files/dirs
				(opts.files && exports.isFile(rel)) ||
				(opts.dirs && exports.isDirectory(rel))
			)
			.map((f) => opts.meta ? exports.getMetadata(f, opts) : f => f) // mapping to their metadata
	)
};

/*** BULK METHOD VERSIONS ***/
exports.listAll = async function (paths, options) {
	var opts = {};
	Object.assign(opts, options);
	return await Promise.all(
		paths.map(path => exports.list(path, opts))
	);
}

exports.getMetadataAll = async function (paths, options) {
	var opts = {};
	Object.assign(opts, options);
	return await Promise.all(
		paths.map(path => exports.getMetadata(path, opts))
	);
}

// merging metadata from file system and database info single object
exports.getMergedMeta = async function (paths, options) {
	var opts = {};
	Object.assign(opts, options);
	var errors = [];
	var metaArr = await fsutils.getMetadataAll(paths, opts).catch(e => {
		console.error(e);
		errors.push(e);
	});
	var fsArr = await db.getFileIndices(paths, opts).catch(e => {
		console.error(e);
		errors.push(e);
	});
	if (errors.length)
		throw { errors: errors };
	const map = new Map();
	metaArr.forEach(item => map.set(item.path, item));
	fsArr.forEach(item => map.set(item.path, { ...map.get(item.path), ...item }));
	const mergedArr = Array.from(map.values());
	return mergedArr;
}
