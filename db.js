// TODO: replace with actual MongoDB
const Datastore = require('nedb');
const fs = require('fs');
const path = require('path');
const bcrypt = require('bcrypt');

exports.idxdb = new Datastore({ filename: 'db/index.db', autoload: true });
exports.usrdb = new Datastore({ filename: 'db/users.db', autoload: true });

// function for seeding data into db from config file
exports.seedUserDb = function (config) {
	fs.readFile(config, 'utf8', (err, users) => {
		JSON.parse(users).forEach(async (user) => {
			var doc = await exports.getUser(user.user);
			// do not update password hash
			// because that inserts a new document
			// and fucks up a DB
			if (!doc) {
				user.pass = bcrypt.hashSync(user.pass, 10);
				exports.usrdb.update(
					{ user: user.user }, // update matching
					user, // what to update with
					{ upsert: !doc }, // only upsert if not exists
					(err) => {
						if (err)
							console.error(err);
						console.log(`Upserted user ${user.user}`);
					}
				)
			}
		});
	});
}

// method for seeding or migrating DB index
exports.seedIndexDb = function (source) {
	fs.readFile(source, 'utf8', (err, paths) => {
		JSON.parse(paths).forEach(async (path) => {
			var doc = await exports.getFileIndices([path.path]);
			if (!doc)
				exports.idxdb.update(
					{ path: path.path }, // update matching
					path, // what to update with
					{ upsert: true }, // only add if not exists
					(err) => {
						if (err)
							console.error(err);
						console.log(`Upserted path ${path.path}`);
					}
				)
		});
	});
}


// gets indexed files for paths
exports.getFileIndices = async function (paths) {
	return await new Promise((resolve, reject) =>
		exports.idxdb.find({
			path: { $in: paths } // match any of arg paths
		}, async (err, docs) => {
			if (err) {
				reject(err);
				return;
			}
			// append paths for which index does not yet exist
			paths.filter(p =>
				!docs.some(i =>
					i.path == p
				)
			).forEach(p =>
				docs.push({
					path: p,
					indexed: false
				})
			);
			resolve(docs);
		})
	);
}

// checks whether paths are locked for users
// returns list of all possible combinations of users and paths
// size is users.length * paths.length
// { user: 'a', path: 'b', locked: true/false }
exports.getPathsPermittedForUsers = async function (paths, users) {
	var relations = [];
	// double loop
	var indices = await exports.getFileIndices(paths);
	indices.forEach(fileIndex =>
		users.forEach(user =>
			relations.push({
				user: user,
				path: fileIndex.path,
				permitted: user && (!fileIndex.owner || fileIndex.owner == user),
			})
		)
	);
	return relations;
}

exports.getPathPermittedForUser = async function (path, user) {
	return (await exports.getPathsPermittedForUsers([path], [user]))[0];
}

// returns users by usernames
// by default, passwords are not returned
exports.getUsers = async function (usernames, options) {
	var opts = { pass: false };
	Object.assign(opts, options);
	return new Promise((resolve, reject) =>
		exports.usrdb.find({ user: { $in: usernames } }, (err, docs) => {
			if (err)
				reject(err);
			else
				resolve(docs.map(doc => { if (!opts.pass) delete doc.pass; return doc }));
		})
	)
}

exports.getUser = async function (username, options) {
	var opts = {};
	Object.assign(opts, options);
	return (await exports.getUsers([username], options))[0];
}

// returns metadata about file from index given user as context
exports.getPathsMetaForUser = async function (paths, user) {
	//exports.getFileIndices(paths)
	return await new Promise((resolve, reject) =>
		exports.idxdb.find({ path: { $in: paths } }, async (err, docs) => {
			if (err)
				reject(err);
			else {
				resolve(
					await Promise.all(docs.map(async doc => ({ // populating obj
						context: user,
						owner: doc.owner,
						path: doc.path,
						permitted: (await exports.getPathPermittedForUser(doc.path, user)).permitted,
						loved: (await exports.getUser(user)).collections.loved.paths.includes(doc.path)
					})))
				);
			}
		})
	);
}

// pin to collection, probably the most complex query in whole app
exports.pin = async function (paths, collections, user, options) {
	var opts = { action: 'add' };
	Object.assign(opts, options);
	// $addToSet : {'collection': {$each: [...]} }
	var changes = Object.fromEntries(new Map(
		collections.map(collection => {
			var operation = {};
			if (opts.action == 'add')
				operation['$each'] = paths;
			if (opts.action == 'remove')
				operation['$in'] = paths;
			return [
				`collections.${collection}.paths`,
				operation
			]
		})
	));
	var query = {};
	if (opts.action == 'add')
		query['$addToSet'] = changes;
	if (opts.action == 'remove')
		query['$pull'] = changes;
	console.dir(query, { depth: null });
	return await new Promise((resolve, reject) =>
		exports.usrdb.update(
			{ user: user, },
			query,
			{ upsert: true, multi: true },
			(err, doc) => {
				if (err)
					reject(err);
				else
					resolve(doc);
			}
		)
	);
}

exports.unpin = async function (paths, collections, user, options) {
	var opts = { action: 'remove' };
	Object.assign(opts, options);
	return await exports.pin(paths, collections, user, opts);
}

// TESTS
//exports.getPathsPermittedForUsers(['2018.jpg', '2017.jpg'], ['mama', 'bernard', 'pia', 'tata', 'leticija']).then(console.dir);
//exports.getUser('bernard').then(u => console.dir(u.collections.loved.paths));
//exports.getPathsMetaForUser(['2018.jpg'], 'mama').then(m => { console.log('metaforuser'); console.dir(m) });
// exports.pin(['a', 'b', 'c'], ['loved', 'wtf', 'cool'], 'bernard')
// 	 .then(res => exports.unpin(['c'], ['cool'], 'bernard'));
