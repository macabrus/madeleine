const { body, query, validationResult } = require('express-validator');
const { isDirectory, isFile } = require('./fsutils');
const { usrdb } = require('./db');

// function to break validation chain and error out
function accumulateErrors(req, res, next) {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(400).json({
			errors: errors.array()
		});
	}
	next();
}

// list endpoint validation logic
const listingPath = [
	body('paths', 'Paths array must be provided').isArray(),
	body('options', "At least one of properties (dirs, files) must be set to true")
		.custom((options) => options.dirs || options.files),
	accumulateErrors
]

// validating request before adding to users collection
const addingToCollection = [
	query('path', 'Path query is mandatory').exists().not().isEmpty(),
	//query('path', 'Path must be file').custom(isFile),
	query('collection').exists().not().isEmpty(),
	accumulateErrors
]

// validation before tagging file/folder
const taggingPath = [
	query('path', 'Path query is mandatory').exists().not().isEmpty(),
	query('tag', 'Tag text must be provided').exists().not().isEmpty(),
	accumulateErrors
]

const lockingPath = [
	query('path', 'Path query is mandatory').exists().not().isEmpty(),
	query('path').custom(isDirectory),
	accumulateErrors
]

const unlockingPath = [
	query('path', 'Path query is mandatory').exists().not().isEmpty(),
	query('path').custom(isDirectory),
	accumulateErrors
]

const gettingFile = [

]

const gettingProfile = [

]


module.exports = function (route) {
	switch (route) {
		case 'list':
			return listingPath
		case 'add':
			return addingToCollection
		case 'tag':
			return taggingPath
		case 'lock':
			return lockingPath
		case 'unlock':
			return unlockingPath
		case 'get':
			return gettingFile
		case 'profile':
			return gettingProfile
		default:
			return []
	}
}