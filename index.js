const path = require('path');
const fs = require('fs');
const express = require('express');
const session = require('express-session');
const favicon = require('serve-favicon')
const FileStore = require('session-file-store')(session);
const bodyParser = require('body-parser');
const ejs = require('ejs');
const app = express();
const port = 3000;
const bcrypt = require('bcrypt')
const { idxdb, usrdb, seedUserDb, seedIndexDb, getUser } = require('./db');
const api = require('./api');


// Upserting from file into db
seedUserDb(path.join(__dirname, 'users.json'));

// optional seeding Index
// Could be used when importing data from another app instance
// Importing index into db
seedIndexDb(path.join(__dirname, 'data.json'));

app.use(favicon(path.join(__dirname, 'static', 'img', 'favicon.ico')))

// view engine setup
app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'ejs');

//setup public folder
app.use('/static', express.static(path.join(__dirname, 'static')));

// set of temporarily blocked IP addresses
const blockedIps = new Set();

// IP check middleware
app.use(function (req, res, next) {
	if (blockedIps.has(req.ip)) {
		console.log(blockedIps);
		res.render('403');
		return;
	}
	next();
});

// Include current route in request object
app.use(function (req, res, next) {
	res.locals.activeNavLink = req.path;
	next();
});

// prepare empty alerts array
// could variable times when alerts persist
// could also implement toasts...
app.use(function (req, res, next) {
	res.locals.alerts = (res.locals.alerts || []).filter(alert =>
		alert.duration && alert.duration--
	);
	res.locals.alerts = [];
	next();
});

// file store session
app.use(session({
	secret: 'password',
	resave: false,
	saveUninitialized: false,
	store: new FileStore,
	cookie: { maxAge: 3600000, secure: false, httpOnly: true }
}));

// for parsing form
app.use(bodyParser.urlencoded({ extended: true }));

//redirect to login if not auth
app.use(function (req, res, next) {
	if (!req.session.user && !req.path.startsWith('/login'))
		res.redirect('/login');
	else
		next();
});

// Bind username to local var for EJS
app.use(function (req, res, next) {
	res.locals.user = req.session.user;
	res.locals.firstName = req.session.firstName;
	next();
});

// Attach API router
app.use('/api', require('./api'));


// login GET middleware
app.get('/login', function (req, res) {
	console.dir(res.locals);
	console.dir(req.session)
	if (req.session.failedLogin) {
		delete req.session.failedLogin;
		res.locals.alerts.push({
			type: 'danger',
			msg: 'Korisničko ime ili lozinka nisu ispravni. Molimo, pokušajte ponovno.'
		});
	}
	// anti-spam measure - TODO
	if (req.session.attempts > 3)
		// Set message of failed login and cooldown
		res.locals.alerts.push({
			type: 'danger',
			msg: 'Priješli ste maksimalan broj pokušaja (3). Molimo, pričekajte nekoliko minuta.'
		});
	res.render('login');
});


// login POST middleware
app.post('/login', function (req, res) {
	if (req.session.attempts > 3) {
		blockedIps.add(req.ip);
		setTimeout(() => { blockedIps.delete(req.ip); }, 5000);
	}
	getUser(req.body.user, { pass: true }).then(doc => {
		console.log(doc);
		if (doc && doc.user == req.body.user && bcrypt.compareSync(req.body.pass, doc.pass)) {
			delete req.session.failedLogin;
			delete req.session.attempts;
			if (req.body.remember) {
				console.log('Users cookie - expires == false');
				req.session.cookie.expires = false;
			}
			req.session.user = doc.user;
			req.session.firstName = doc.firstName;
			res.redirect('/')
		}
		else {
			if (!req.session.attempts)
				req.session.attempts = 0;
			req.session.attempts++;
			req.session.failedLogin = true;
			res.redirect('/login');
		}
	});
});

// deauth user on logout
app.post('/logout', function (req, res) {
	req.session.regenerate((err) => {
		if (err)
			console.error(err);
		res.redirect('/login');
	})
});

// profile details
app.get('/profile', (req, res) => {
	getUser(req.query.q).then(profile => {
		res.render('profile', profile);
	});
});

app.get('/collections', function (req, res) {
	res.render('collections')
});

// local variables for breadcrumbs & path
// only for home route
app.use('/', function (req, res, next) {
	// remembering current path in session
	// so if none is provided as query parameter
	// last one will be used from session
	if (req.query.path) {
		req.session.paramPath = req.query.path;
		req.session.breadcrumbs = [];
		var pathBuilder = '';
		var arr = req.query.path.split('/');
		if (arr[0])
			arr.unshift('')
		if (arr[arr.length - 1])
			arr.push('')
		for (part of arr) {
			pathBuilder = path.join(pathBuilder, part);
			req.session.breadcrumbs.push({ path: pathBuilder, name: part });
		}
	}
	res.locals.paramPath = req.session.paramPath;
	res.locals.breadcrumbs = req.session.breadcrumbs || [{ path: '', name: '' }];
	next();
});

// finally, if none of the above -> show home
app.get("/", function (req, res) {
	res.render('home');
});

app.listen(port, () => {
	console.log(`Madeleine live at http://localhost:${port}`)
});
